<?php

/** 
 * Androgogic Support Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     07/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new support_log
 *
 **/

global $OUTPUT;

require_capability('block/androgogic_support:view', $context);

require_once('support_log_edit_form.php');
$mform = new support_log_edit_form();
if ($data = $mform->get_data()){
    $data->user_id = $USER->id;
    $data->site_name = $SITE->fullname;    
    $data->date_created = date('Y-m-d H:i:s');
    $data->steps_to_reproduce = format_text($data->steps_to_reproduce['text'], $data->steps_to_reproduce['format']);
    //fix file entries
    $sql = "UPDATE mdl_files
    SET filearea = 'content',component='block_androgogic_support'
    WHERE itemid = '$data->uploaded_file_id'";
    $DB->execute($sql);
    $newid = $DB->insert_record('androgogic_support_log',$data);
    // send email to support
    $support->firstname = 'Support';
    $support->lastname = 'at-Androgogic';
    $support->email = $CFG->support_to_email_address;
    $from = $USER; //this will need to change for non-logged in users
    $replyto = $USER->email; //$CFG->support_replyto_email_address;
    
    $profile_url = $CFG->wwwroot.'/user/profile.php?id='.$USER->id;
    $midnight_this_morning = mktime( 0, 0, 0, date("m"), date("d"), date("Y"));
    $recent_activities_url = $CFG->wwwroot.'/report/log/index.php?chooselog=1&showusers=1&showcourses=1&id=1&user='.$USER->id.'&date='.$midnight_this_morning.'&modid=&modaction=&logformat=showashtml';
    
    $time_of_request = date('d M Y H:i:s');
    //send the email    
    $subject = $data->problem_description;
    $message = 'The support form has been submitted on the website "' . $data->site_name . '"' ."\r\n";
    $message .= 'Time and date of submission: '.$time_of_request . "\r\n";
    $message .= "\r\n";
    $message .= 'Submitted information: ' . "\r\n" ;
    foreach($_POST as $key=>$value){
        if($key != 'steps_to_reproduce'){
            $message .= $key . ': ' . $value. "\r\n";
        }
    }
    $message .= "steps_to_reproduce: " . $data->steps_to_reproduce;
    $message .= "\r\n";
    $message .= "\r\n";
    if(isloggedin()){
        $message .= 'The user was logged in.'. "\r\n";
        $message .= 'Link to their profile: '. $profile_url . "\r\n";
        $message .= 'Link to their recent activities report: '. $recent_activities_url . "\r\n";
    }
    else{
        $message .= 'The user was not logged in.'. "\r\n";
    }
    $message .= "\r\n";
    $message .= 'Ancillary information: ' . "\r\n";
    $desired_server_params = array('HTTP_HOST','HTTP_USER_AGENT','SCRIPT_FILENAME','REMOTE_ADDR','HTTP_ACCEPT_LANGUAGE');
    foreach($_SERVER as $key=>$value){
        if(in_array($key, $desired_server_params)){
            $message .= $key . ': ' . $value. "\r\n";
        }
    }
    $attachment = '';
    $attachname = '';
    $q = "select * from mdl_files where itemid = '$data->uploaded_file_id' and filename != ''  and filename != '.'";
    $file = $DB->get_record_sql($q);
    if($file){
        $attachment = androgogic_support_path_from_hash($file->contenthash).'/'.$file->contenthash;
        $attachname = $file->filename;
    }
    email_to_user($support, $from, $subject, $message, $message, $attachment, $attachname, true, $replyto, '');
    echo $OUTPUT->notification(get_string('support_request_submitted','block_androgogic_support'), 'notifysuccess');
    //echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('support_log_new', 'block_androgogic_support'));
$mform->display();
}

?>
